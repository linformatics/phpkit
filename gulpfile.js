var _       = require('lodash'),
    zip     = require('gulp-zip'),
    gulp    = require('gulp'),
    file    = require('gulp-file'),
    phpcs   = require('gulp-phpcs'),
    phplint = require('phplint').lint,
    phpunit = require('gulp-phpunit'),

    pkg = require('./package.json');

gulp.task('lint', ['phplint', 'phpcs']);

gulp.task('phplint', function (cb) {
    phplint(['./src/**/*.php', './tests/**/*.php'], {limit: 10}, function (err) {
        if (err) {
            cb(err);
            process.exit(1);
        }
        cb();
    });
});

gulp.task('phpcs', function () {
    return gulp.src('./src/**/*.php')
        .pipe(phpcs({
            bin: process.env.PHPCS || '../../vendor/bin/phpcs',
            standard: 'standards/fw_phpcs',
            colors: true
        }))
        .pipe(phpcs.reporter('log'))
        .pipe(phpcs.reporter('fail'));
});

gulp.task('phpunit', function () {
    gulp.src('./phpunit.xml')
        .pipe(phpunit(process.env.PHPUNIT || '../../vendor/bin/phpunit', {debug: false, notify: true, silent: true}));
});

gulp.task('validate', ['lint', 'phpunit']);

gulp.task('fw:build', function () {
    var name = _.map(pkg.name.split(/-/), function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }).join('');

    return file('fw.json', JSON.stringify({
            name: name,
            version: pkg.version
        }), {src: true})
        .pipe(gulp.dest('./'));
});

gulp.task('release', ['fw:build'], function () {
    gulp.src([
        'src/**/*.php',
        'fw.json',
        'composer.json',
        'composer.lock'
    ], {base: './'})
        .pipe(zip(pkg.name.toLowerCase() + '-' + pkg.version + '.zip'))
        .pipe(gulp.dest('./'));
});
